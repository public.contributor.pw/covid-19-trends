/**
 *
 */
function getData() {
  const httpResponse = UrlFetchApp.fetch(
    'https://pomber.github.io/covid19/timeseries.json'
  );
  const data = JSON.parse(
    Utilities.newBlob(
      httpResponse.getContentText(),
      'application/json'
    ).getDataAsString()
  );
  const values = Object.keys(data).reduce((vals, country) => {
    const items = data[country];
    vals = vals.concat(
      items.map((item) => [
        country,
        item.date,
        item.confirmed,
        item.deaths,
        item.recovered,
      ])
    );
    return vals;
  }, []);
  return values;
}

/**
 *
 */
function setData() {
  const data = getData();
  const sheet = SpreadsheetApp.openById(
    '1qNl4y-PZZrXTvpIX61d_EvglJxjjfKmSJ53C_IT_GaI'
  ).getSheetByName('pomber');
  sheet
    .clearContents()
    .getRange(2, 1, data.length, data[0].length)
    .setValues(data)
    .getSheet()
    .getRange(1, 1, 1, 5)
    .setValues([['Country', 'Date', 'Confirmed', 'Deaths', 'Recovered']]);
}
