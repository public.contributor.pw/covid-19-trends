/* exported App */
/**
 * App
 * @class App
 * @constructor
 * @public
 */
class App {
  /**
   *
   * @param {GoogleAppsScript.Spreadsheet.Spreadsheet} book
   */
  constructor(book = SpreadsheetApp.getActive()) {
    if (!this._instance) {
      /**
       * @type {GoogleAppsScript.Spreadsheet.Spreadsheet}
       */
      this.book = book;
      /**
       * @type {App.settings}
       */
      this.settings = {};
      /**
       * @type {App.templates}
       */
      this.templates = {};
      this.init();
      this._instance = this;
    }
    return this._instance;
  }
  /**
   *
   */
  init() {
    this.settings = this.book
      .getSheetByName('Settings')
      .getDataRange()
      .getValues()
      .reduce((settings, row) => {
        const [key, value] = row;
        const _key_ = ('' + key).toLowerCase();
        if (
          _key_ !== '' &&
          !Object.prototype.hasOwnProperty.call(settings, _key_)
        )
          settings[_key_] = value;
        return settings;
      }, {});
    this.templates = this.book
      .getSheets()
      .filter((sheet) => /^template/i.test(sheet.getName()))
      .reduce((templates, sheet) => {
        const key = sheet.getName().toLowerCase();
        templates[key] = { sheet };
        return templates;
      }, {});
  }
  /**
   *
   * @param {string} country
   */
  updateCountryFormulas(country) {
    const sheet = this.book.getSheetByName(country);

    if (
      sheet &&
      this.settings['allowed to update formulas'].includes(country) &&
      this.templates['template. country']
    ) {
      const values = this.templates['template. country'].sheet
        .getRange('A1:L2')
        .getValues();
      sheet.clearContents();
      sheet.getRange('A1:L2').setValues(values);
      const staticFormulas = this.templates['template. country'].sheet
        .getRange('A3:L3')
        .getFormulas()
        .map((row) => row.map((cell) => cell.replace('Russia', country)));
      sheet.getRange('A3:L3').setFormulas(staticFormulas);
      SpreadsheetApp.flush();
      this.templates['template. country'].sheet
        .getRange('F4:L4')
        .copyTo(
          sheet.getRange(4, 6, sheet.getLastRow() - 3, 7),
          SpreadsheetApp.CopyPasteType.PASTE_FORMULA
        );
      this.templates['template. country'].sheet
        .getRange('G5:H8')
        .copyTo(
          sheet.getRange(sheet.getLastRow(), 7, 4, 2),
          SpreadsheetApp.CopyPasteType.PASTE_FORMULA
        );
    }
  }
}

function testss() {}

/**
 * @typedef {Object.<string, any>} App.settings
 */

/**
 * @typedef {{sheet: GoogleAppsScript.Spreadsheet.Sheet}} App.template
 */

/**
 * @typedef {Object.<string, App.template>} App.templates
 */
