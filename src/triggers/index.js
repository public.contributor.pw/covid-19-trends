function onOpen() {
  SpreadsheetApp.getUi()
    .createMenu('COVID-19 trends')
    .addItem('Update current country', 'userActionUpdateCurrentCountry')
    .addToUi();
}

function userActionUpdateCurrentCountry() {
  new App().updateCountryFormulas(SpreadsheetApp.getActiveSheet().getName());
}
